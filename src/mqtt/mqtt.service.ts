import { Injectable } from '@nestjs/common';
import { connect, MqttClient } from 'mqtt';
import { MqttDataDto } from './mqtt.dto';
import { DadosService } from '../dados/dados.service';
import { ConfiguracaoService } from '../configuracao/configuracao.service';
import { PlantacaoService } from '../plantacao/plantacao.service';

@Injectable()
export class MqttService {
  private client: MqttClient;

  constructor(
    private readonly dadosService: DadosService,
    private readonly configuracaoService: ConfiguracaoService,
    private readonly plantacaoService: PlantacaoService,
  ) {
    this.connectToBroker();
  }

  private connectToBroker() {
    this.client = connect('mqtt://test.mosquitto.org:1883', {
      reconnectPeriod: 10000,
    });

    this.client.on('connect', () => {
      console.log('Connected to MQTT broker');
      this.subscribeToIrrigajaTopic();
    });

    this.client.on('reconnect', () => {
      console.log('Reconnecting to MQTT broker');
    });

    this.client.on('error', (err: Error) => {
      console.error('Error:', err);
    });

    this.client.on('close', () => {
      console.log('Connection to MQTT broker closed');
    });

    this.client.on('message', (topic: string, message) => {
      this.handleMqttMessage(topic, message.toString());
    });
  }

  private subscribeToIrrigajaTopic() {
    this.client.subscribe('irrigaja/+/data', (err: Error | null, granted) => {
      if (err) {
        console.error('Erro ao inscrever-se em irrigaja/+/data:', err);
      } else {
        console.log('Inscrição bem-sucedida em irrigaja/+/data:', granted);
      }
    });
  }

  private handleMqttMessage(topic: string, message: string) {
    try {
      const esp32_id = topic.split('/')[1];
      const data: MqttDataDto = JSON.parse(message);
      console.log(message);
      data.esp32_id = esp32_id;
      data.timestamp = new Date();
      this.saveDataToDatabase(data);
      this.verificaLigaDesligaBomba(esp32_id, data.soil_humidity);
    } catch (error) {
      console.error('Erro ao processar mensagem MQTT:', error);
    }
  }

  private async saveDataToDatabase(data: MqttDataDto) {
    try {
      const createdDado = await this.dadosService.create({
        plantacao_id: data.esp32_id,
        velocidade_ar: data.wind_speed,
        umidade_ar: data.air_humidity,
        umidade_solo: data.soil_humidity,
        temperatura: data.temperature,
        registro: data.timestamp,
        status: data.status,
        reservatorio: data.reservatorio,
      });

      console.log('Data saved to database:', createdDado);

      const plantacao = await this.plantacaoService.findOne(data.esp32_id);
      plantacao.status = data.status;

      this.plantacaoService.update(data.esp32_id, {
        status: data.status,
        manual: plantacao.manual,
      });
    } catch (error) {
      console.error('Error saving data to database:', error.message);
    }
  }

  private async verificaLigaDesligaBomba(
    idPlantacao: string,
    dadoUmidadeSolo: number,
  ) {
    try {
      const configuracao =
        await this.configuracaoService.findOneByPlantacao(idPlantacao);

      const topic = `irrigaja/${idPlantacao}/commands/waterPump`;
      if (configuracao.plantacao.manual == false) {
        if (dadoUmidadeSolo < configuracao.umidade_solo_min) {
          this.publish(topic, JSON.stringify({ command: 'on' }));
        } else {
          this.publish(topic, JSON.stringify({ command: 'off' }));
        }
      }
    } catch (error) {
      console.error('Erro ao buscar plantação:', error.message);
    }
  }

  publish(topic: string, message: string) {
    try {
      this.client.publish(topic, message);
    } catch (error) {
      console.error('Error while publishing message:', error);
    }
  }
}
