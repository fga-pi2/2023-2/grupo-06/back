export class MqttDataDto {
  esp32_id: string;
  wind_speed: number;
  air_humidity: number;
  soil_humidity: number;
  temperature: number;
  timestamp: Date;
  status: boolean;
  reservatorio: boolean;
}
