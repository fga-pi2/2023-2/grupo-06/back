import { Controller, Post, Body, Param } from '@nestjs/common';
import { MqttService } from './mqtt.service';

@Controller('mqtt')
export class MqttController {
  constructor(private readonly mqttService: MqttService) {}

  @Post(':esp32_id/control')
  controlWaterPump(
    @Param('esp32_id') esp32_id: string,
    @Body() body: { command: 'on' | 'off' },
  ) {
    try {
      const { command } = body;
      this.mqttService.publish(
        `irrigaja/${esp32_id}/commands/waterPump`,
        JSON.stringify(body),
      );
      return `Sending command to ESP32: ${command}`;
    } catch (error) {
      console.error('Error in controlWaterPump:', error);
      return 'Failed to send command to ESP32';
    }
  }

  @Post(':esp32_id/registrationConfig')
  sendRegistrationConfig(
    @Param('esp32_id') esp32_id: string,
    @Body()
    body: {
      temp_max: number;
      temp_min: number;
      humidity_max: number;
      humidity_min: number;
    },
  ) {
    try {
      this.mqttService.publish(
        `irrigaja/${esp32_id}/commands/registrationConfig`,
        JSON.stringify(body),
      );

      return `Sending registration config to ESP32 ${esp32_id}: ${JSON.stringify(
        body,
      )}`;
    } catch (error) {
      console.error('Error in sendRegistrationConfig:', error);
      return 'Failed to send registration config to ESP32';
    }
  }
}
