import { Test, TestingModule } from '@nestjs/testing';
import { MqttController } from './mqtt.controller';
import { MqttService } from './mqtt.service';

describe('MqttController', () => {
  let mqttController: MqttController;
  let mqttService: MqttService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MqttController],
      providers: [
        {
          provide: MqttService,
          useValue: {
            publish: jest.fn(),
          },
        },
      ],
    }).compile();

    mqttController = module.get<MqttController>(MqttController);
    mqttService = module.get<MqttService>(MqttService);
  });

  describe('controlWaterPump', () => {
    it('should send command to ESP32', () => {
      const esp32_id = 'esp32_1';
      const body: { command: 'on' | 'off' } = { command: 'on' };

      mqttController.controlWaterPump(esp32_id, body);

      expect(mqttService.publish).toHaveBeenCalledWith(
        `irrigaja/${esp32_id}/commands/waterPump`,
        JSON.stringify(body),
      );
    });
  });

  describe('sendRegistrationConfig', () => {
    it('should send registration config to ESP32', () => {
      const esp32_id = 'esp32_2';
      const body = {
        temp_max: 30,
        temp_min: 20,
        humidity_max: 80,
        humidity_min: 60,
      };

      mqttController.sendRegistrationConfig(esp32_id, body);

      expect(mqttService.publish).toHaveBeenCalledWith(
        `irrigaja/${esp32_id}/commands/registrationConfig`,
        JSON.stringify(body),
      );
    });
  });
});
