import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MqttController } from './mqtt.controller';
import { MqttService } from './mqtt.service';
import { DadosService } from '../dados/dados.service';
import { PlantacaoService } from '../plantacao/plantacao.service';
import { Dado } from '../dados/entities/dado.entity';
import { Plantacao } from '../plantacao/entities/plantacao.entity';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';
import { ConfiguracaoService } from '../configuracao/configuracao.service';
import { Configuracao } from '../configuracao/entities/configuracao.entity';
@Global()
@Module({
  imports: [TypeOrmModule.forFeature([Dado, Plantacao, User, Configuracao])],
  controllers: [MqttController],
  providers: [
    MqttService,
    DadosService,
    PlantacaoService,
    UsersService,
    ConfiguracaoService,
  ],
  exports: [MqttService],
})
export class MqttModule {}
