import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { PlantacaoModule } from './plantacao/plantacao.module';
import { DadosModule } from './dados/dados.module';
import { ConfiguracaoModule } from './configuracao/configuracao.module';
import { PresetModule } from './preset/preset.module';
import config from './config/config';
import { MqttModule } from './mqtt/mqtt.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(config),
    UsersModule,
    PlantacaoModule,
    DadosModule,
    ConfiguracaoModule,
    PresetModule,
    MqttModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
