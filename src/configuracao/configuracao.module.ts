import { Module } from '@nestjs/common';
import { ConfiguracaoService } from './configuracao.service';
import { ConfiguracaoController } from './configuracao.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Configuracao } from './entities/configuracao.entity';
import { PlantacaoService } from 'src/plantacao/plantacao.service';
import { Plantacao } from 'src/plantacao/entities/plantacao.entity';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';

@Module({
  imports: [TypeOrmModule.forFeature([Configuracao, Plantacao, User])],
  controllers: [ConfiguracaoController],
  providers: [ConfiguracaoService, PlantacaoService, UsersService],
})
export class ConfiguracaoModule {}
