import { Plantacao } from '../../plantacao/entities/plantacao.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Configuracao {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false, type: 'decimal' })
  umidade_solo_max: number;

  @Column({ nullable: false, type: 'decimal' })
  umidade_ar_max: number;

  @Column({ nullable: false, type: 'decimal' })
  velocidade_ar_max: number;

  @Column({ nullable: false, type: 'decimal' })
  temperatura_max: number;

  @Column({ nullable: false, type: 'decimal' })
  umidade_solo_min: number;

  @Column({ nullable: false, type: 'decimal' })
  umidade_ar_min: number;

  @Column({ nullable: false, type: 'decimal' })
  velocidade_ar_min: number;

  @Column({ nullable: false, type: 'decimal' })
  temperatura_min: number;

  @Column({ nullable: false, type: 'varchar', length: '100' })
  nome: string;

  @ManyToOne(() => Plantacao, (plantacao) => plantacao.configuracao)
  @JoinColumn()
  plantacao: Plantacao;
}
