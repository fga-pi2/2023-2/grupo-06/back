import { Test, TestingModule } from '@nestjs/testing';
import { ConfiguracaoController } from './configuracao.controller';
import { ConfiguracaoService } from './configuracao.service';
import { CreateConfiguracaoDto } from './dto/create-configuracao.dto';

describe('ConfiguracaoController', () => {
  let controller: ConfiguracaoController;

  const mockCreateConfiguracaoDto: CreateConfiguracaoDto = {
    umidade_solo_max: 50,
    umidade_ar_max: 25,
    velocidade_ar_max: 100,
    temperatura_max: 50,
    umidade_solo_min: 50,
    umidade_ar_min: 25,
    velocidade_ar_min: 100,
    temperatura_min: 50,
    nome: 'Configuracao 1',
    plantacao_id: '1640237604',
  };

  const mockConfiguracaoService = {
    create: jest.fn((dto) => {
      return {
        ...dto,
      };
    }),
    findOne: jest.fn((id) => {
      return {
        ...mockCreateConfiguracaoDto,
        id,
      };
    }),
    update: jest.fn((id, dto) => {
      return {
        ...mockCreateConfiguracaoDto,
        ...dto,
      };
    }),
    remove: jest.fn(() => {
      return {
        message: 'Configuracao removida com sucesso',
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ConfiguracaoController],
      providers: [ConfiguracaoService],
    })
      .overrideProvider(ConfiguracaoService)
      .useValue(mockConfiguracaoService)
      .compile();

    controller = module.get<ConfiguracaoController>(ConfiguracaoController);
  });

  describe('configuracao', () => {
    it('should be defined', () => {
      expect(controller).toBeDefined();
    });
  });
});
