import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Configuracao } from './entities/configuracao.entity';
import { CreateConfiguracaoDto } from './dto/create-configuracao.dto';
import { UpdateConfiguracaoDto } from './dto/update-configuracao.dto';
import { PlantacaoService } from '../plantacao/plantacao.service';

@Injectable()
export class ConfiguracaoService {
  constructor(
    @InjectRepository(Configuracao)
    private configuracaoRepository: Repository<Configuracao>,
    private plantacaoService: PlantacaoService,
  ) {}

  async create(createConfiguracaoDto: CreateConfiguracaoDto) {
    // Search plantacao by plantacao_id using try/catch
    try {
      const plantacao = await this.plantacaoService.findOne(
        createConfiguracaoDto.plantacao_id,
      );

      try {
        const configuracao = this.configuracaoRepository.create({
          ...createConfiguracaoDto,
          plantacao,
        });

        return await this.configuracaoRepository.save(configuracao);
      } catch (error) {
        throw new Error('Erro ao criar configuração');
      }
    } catch (error) {
      throw new Error('Plantacao não encontrada');
    }
  }

  async findAll() {
    return this.configuracaoRepository.find();
  }

  async findOne(id: string) {
    return this.configuracaoRepository.findOneBy({ id: id });
  }

  async findOneByPlantacao(idPlantacao: string) {
    return this.configuracaoRepository.findOne({
      where: { plantacao: { id: idPlantacao } },
      relations: ['plantacao'],
    });
  }

  async update(id: string, updateConfiguracaoDto: UpdateConfiguracaoDto) {
    await this.configuracaoRepository.update(id, updateConfiguracaoDto);
    return this.configuracaoRepository.findOneBy({ id: id });
  }

  async remove(id: string) {
    const configuracao = await this.configuracaoRepository.findOneBy({
      id: id,
    });
    return this.configuracaoRepository.remove(configuracao);
  }
}
