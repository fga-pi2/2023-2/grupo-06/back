import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { ConfiguracaoService } from './configuracao.service';
import { CreateConfiguracaoDto } from './dto/create-configuracao.dto';
import { UpdateConfiguracaoDto } from './dto/update-configuracao.dto';

@Controller('configuracao')
export class ConfiguracaoController {
  constructor(private readonly configuracaoService: ConfiguracaoService) {}

  @Post()
  async create(@Body() createConfiguracaoDto: CreateConfiguracaoDto) {
    return await this.configuracaoService.create(createConfiguracaoDto);
  }

  @Get()
  async findAll() {
    return await this.configuracaoService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.configuracaoService.findOne(id);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateConfiguracaoDto: UpdateConfiguracaoDto,
  ) {
    return await this.configuracaoService.update(id, updateConfiguracaoDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.configuracaoService.remove(id);
  }
}
