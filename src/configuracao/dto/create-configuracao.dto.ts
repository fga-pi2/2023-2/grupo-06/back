import { IsNotEmpty, MaxLength } from 'class-validator';

export class CreateConfiguracaoDto {
  @IsNotEmpty({ message: 'O campo umidade solo maximo é obrigatório' })
  umidade_solo_max: number;

  @IsNotEmpty({ message: 'O campo umidade ar maximo é obrigatório' })
  umidade_ar_max: number;

  @IsNotEmpty({ message: 'O campo velocidade ar maximo é obrigatório' })
  velocidade_ar_max: number;

  @IsNotEmpty({ message: 'O campo temperatura maximo é obrigatório' })
  temperatura_max: number;

  @IsNotEmpty({ message: 'O campo umidade solo minimo é obrigatório' })
  umidade_solo_min: number;

  @IsNotEmpty({ message: 'O campo umidade ar minimo é obrigatório' })
  umidade_ar_min: number;

  @IsNotEmpty({ message: 'O campo velocidade ar minimo é obrigatório' })
  velocidade_ar_min: number;

  @IsNotEmpty({ message: 'O campo temperatura minimo é obrigatório' })
  temperatura_min: number;

  @IsNotEmpty({ message: 'O campo nome é obrigatório' })
  @MaxLength(100, { message: 'O campo nome deve ter no máximo 100 caracteres' })
  nome: string;

  @IsNotEmpty({ message: 'O ID da plantação não pode estar vazio' })
  plantacao_id: string;
}
