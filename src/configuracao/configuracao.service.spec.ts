import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Configuracao } from './entities/configuracao.entity';
import { ConfiguracaoService } from './configuracao.service';
import { PlantacaoService } from '../plantacao/plantacao.service';

describe('ConfiguracaoService', () => {
  let service: ConfiguracaoService;
  let repository: Repository<Configuracao>;

  const mockCreateConfiguracaoDto = {
    id: '1640237604',
    umidade_solo_max: 50,
    umidade_ar_max: 25,
    velocidade_ar_max: 100,
    temperatura_max: 50,
    umidade_solo_min: 50,
    umidade_ar_min: 25,
    velocidade_ar_min: 100,
    temperatura_min: 50,
    nome: 'Configuracao 1',
    plantacao: {
      id: '1640237604',
      nome: 'Plantacao 1',
      manual: false,
      status: true,
    },
  };

  const mockReturnConfiguracaoDto = {
    ...mockCreateConfiguracaoDto,
  };

  const mockConfiguracaoEntityList = [{ ...mockReturnConfiguracaoDto }];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ConfiguracaoService,
        {
          provide: getRepositoryToken(Configuracao),
          useValue: {
            create: jest.fn().mockReturnValue(new Configuracao()),
            find: jest.fn().mockResolvedValue(mockConfiguracaoEntityList),
            findOneBy: jest.fn().mockResolvedValue(mockReturnConfiguracaoDto),
            merge: jest.fn().mockReturnValue(mockReturnConfiguracaoDto),
            delete: jest.fn(),
            save: jest.fn(),
          },
        },
        {
          provide: PlantacaoService,
          useValue: {
            findOne: jest.fn().mockResolvedValue(mockReturnConfiguracaoDto),
          },
        },
      ],
    }).compile();

    service = module.get<ConfiguracaoService>(ConfiguracaoService);
    repository = module.get<Repository<Configuracao>>(
      getRepositoryToken(Configuracao),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(repository).toBeDefined();
  });
});
