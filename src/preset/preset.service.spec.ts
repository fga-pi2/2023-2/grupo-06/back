import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PresetService } from './preset.service';
import { Preset } from './entities/preset.entity';

describe('PresetService', () => {
  let service: PresetService;
  let repository: Repository<Preset>;

  const mockCreatePresetDto = {
    umidade_solo_max: 50,
    umidade_ar_max: 25,
    velocidade_ar_max: 100,
    temperatura_max: 50,
    umidade_solo_min: 50,
    umidade_ar_min: 25,
    velocidade_ar_min: 100,
    temperatura_min: 50,
    nome: 'Preset 1',
  };

  const mockReturnPresetDto = {
    ...mockCreatePresetDto,
  };

  const mockPresetEntityList = [{ ...mockReturnPresetDto }];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PresetService,
        {
          provide: getRepositoryToken(Preset),
          useValue: {
            create: jest.fn().mockReturnValue(new Preset()),
            find: jest.fn().mockResolvedValue(mockPresetEntityList),
            findOneBy: jest.fn().mockResolvedValue(mockReturnPresetDto),
            merge: jest.fn().mockReturnValue(mockReturnPresetDto),
            delete: jest.fn(),
            save: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<PresetService>(PresetService);
    repository = module.get<Repository<Preset>>(getRepositoryToken(Preset));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(repository).toBeDefined();
  });
});
