import { Module } from '@nestjs/common';
import { PresetService } from './preset.service';
import { PresetController } from './preset.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Preset } from './entities/preset.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Preset])],
  controllers: [PresetController],
  providers: [PresetService],
})
export class PresetModule {
  // Insert data into preset table on initialization

  constructor(private readonly presetService: PresetService) {
    this.presetService.deleteAll();

    this.presetService.create({
      umidade_solo_max: 70,
      umidade_ar_max: 60,
      velocidade_ar_max: 20,
      temperatura_max: 25,
      umidade_solo_min: 30,
      umidade_ar_min: 20,
      velocidade_ar_min: 5,
      temperatura_min: 10,
      nome: 'Alface',
    });

    this.presetService.create({
      umidade_solo_max: 75,
      umidade_ar_max: 65,
      velocidade_ar_max: 25,
      temperatura_max: 28,
      umidade_solo_min: 35,
      umidade_ar_min: 25,
      velocidade_ar_min: 10,
      temperatura_min: 15,
      nome: 'Tomate',
    });

    this.presetService.create({
      umidade_solo_max: 80,
      umidade_ar_max: 55,
      velocidade_ar_max: 15,
      temperatura_max: 30,
      umidade_solo_min: 28,
      umidade_ar_min: 18,
      velocidade_ar_min: 8,
      temperatura_min: 12,
      nome: 'Cenoura',
    });
  }
}

// Alface:

// Umidade do Solo: Alfaces preferem solo úmido, mas não encharcado. A faixa é definida entre 30-70%.
// Umidade do Ar: Boa umidade é crucial para o desenvolvimento das alfaces. A faixa está entre 20-60%.
// Velocidade do Vento: Alfaces podem ser sensíveis ao vento forte, então uma faixa mais baixa, como 5-20 km/h, é considerada.
// Temperatura: Alfaces crescem bem em temperaturas mais frias, geralmente entre 10-25°C.

// Tomate:

// Umidade do Solo: Tomates preferem solo ligeiramente úmido. A faixa está entre 35-75%.
// Umidade do Ar: Tomates podem tolerar umidade mais alta, entre 25-65%.
// Velocidade do Vento: Tomates podem lidar com ventos um pouco mais fortes, entre 10-25 km/h.
// Temperatura: Tomates preferem temperaturas mais quentes, entre 15-28°C.

// Cenoura:

// Umidade do Solo: Cenouras preferem solo úmido, mas não encharcado. A faixa é definida entre 30-80%.
// Umidade do Ar: A faixa está entre 18-55%.
// Velocidade do Vento: Cenouras podem lidar com ventos mais leves, entre 8-15 km/h.
// Temperatura: Cenouras crescem bem em temperaturas moderadas, entre 12-30°C.
