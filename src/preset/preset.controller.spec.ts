import { Test, TestingModule } from '@nestjs/testing';
import { PresetController } from './preset.controller';
import { PresetService } from './preset.service';
import { CreatePresetDto } from './dto/create-preset.dto';

describe('PresetController', () => {
  let controller: PresetController;

  const mockCreatePresetDto: CreatePresetDto = {
    umidade_solo_max: 50,
    umidade_ar_max: 25,
    velocidade_ar_max: 100,
    temperatura_max: 50,
    umidade_solo_min: 50,
    umidade_ar_min: 25,
    velocidade_ar_min: 100,
    temperatura_min: 50,
    nome: 'bananinha',
  };

  const mockPresetService = {
    create: jest.fn((dto) => {
      return {
        ...dto,
      };
    }),
    findOne: jest.fn((id) => {
      return {
        ...mockCreatePresetDto,
        id,
      };
    }),
    update: jest.fn((id, dto) => {
      return {
        ...mockCreatePresetDto,
        ...dto,
      };
    }),
    remove: jest.fn(() => {
      return {
        message: 'Preset removida com sucesso',
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PresetController],
      providers: [PresetService],
    })
      .overrideProvider(PresetService)
      .useValue(mockPresetService)
      .compile();

    controller = module.get<PresetController>(PresetController);
  });

  describe('preset', () => {
    it('should be defined', () => {
      expect(controller).toBeDefined();
    });
  });
});
