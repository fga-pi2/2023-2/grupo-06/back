import { Injectable } from '@nestjs/common';
import { CreatePresetDto } from './dto/create-preset.dto';
import { UpdatePresetDto } from './dto/update-preset.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Preset } from './entities/preset.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PresetService {
  constructor(
    @InjectRepository(Preset)
    private presetRepository: Repository<Preset>,
  ) {}

  async create(createPresetDto: CreatePresetDto) {
    try {
      const preset = this.presetRepository.create({
        ...createPresetDto,
      });

      return await this.presetRepository.save(preset);
    } catch (error) {
      throw new Error('Erro ao criar preset');
    }
  }

  async findAll() {
    return this.presetRepository.find();
  }

  async findOne(id: string) {
    return this.presetRepository.findOneBy({ id: id });
  }

  async update(id: string, updatePresetDto: UpdatePresetDto) {
    await this.presetRepository.update(id, updatePresetDto);
    return this.presetRepository.findOneBy({ id: id });
  }

  async remove(id: string) {
    const preset = await this.presetRepository.findOneBy({
      id: id,
    });
    return this.presetRepository.remove(preset);
  }

  // Delete all presets
  async deleteAll() {
    return this.presetRepository.clear();
  }
}
