import { Module } from '@nestjs/common';
import { DadosService } from './dados.service';
import { DadosController } from './dados.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dado } from './entities/dado.entity';
import { Plantacao } from 'src/plantacao/entities/plantacao.entity';
import { PlantacaoService } from 'src/plantacao/plantacao.service';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';

@Module({
  imports: [TypeOrmModule.forFeature([Dado, Plantacao, User])],
  controllers: [DadosController],
  providers: [DadosService, PlantacaoService, UsersService],
})
export class DadosModule {}
