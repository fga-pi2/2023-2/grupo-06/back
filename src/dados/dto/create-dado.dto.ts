import {
  IsBoolean,
  IsDateString,
  IsDecimal,
  IsNotEmpty,
} from 'class-validator';

export class CreateDadoDto {
  @IsNotEmpty({ message: 'A umidade do solo não pode estar vazia' })
  @IsDecimal(
    { force_decimal: true },
    { message: 'A umidade do solo deve ser um número decimal' },
  )
  umidade_solo: number;

  @IsNotEmpty({ message: 'A umidade do ar não pode estar vazia' })
  @IsDecimal(
    { force_decimal: true },
    { message: 'A umidade do ar deve ser um número decimal' },
  )
  umidade_ar: number;

  @IsNotEmpty({ message: 'A velocidade do ar não pode estar vazia' })
  @IsDecimal(
    { force_decimal: true },
    { message: 'A velocidade do ar deve ser um número decimal' },
  )
  velocidade_ar: number;

  @IsNotEmpty({ message: 'A temperatura não pode estar vazia' })
  @IsDecimal(
    { force_decimal: true },
    { message: 'A temperatura deve ser um número decimal' },
  )
  temperatura: number;

  @IsNotEmpty({ message: 'Insira um valor para o status' })
  @IsBoolean({ message: 'O campo manual deve ser booleano' })
  status: boolean;

  @IsNotEmpty({ message: 'Insira um valor para o reservatório' })
  @IsBoolean({ message: 'O campo manual deve ser booleano' })
  reservatorio: boolean;

  @IsNotEmpty({ message: 'O registro não pode estar vazio' })
  @IsDateString(
    { strict: true },
    { message: 'O registro deve ser uma data válida' },
  )
  registro: Date;

  @IsNotEmpty({ message: 'O ID da plantação não pode estar vazio' })
  plantacao_id: string;
}
