import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { DadosService } from './dados.service';
import { CreateDadoDto } from './dto/create-dado.dto';
import { UpdateDadoDto } from './dto/update-dado.dto';

@Controller('dados')
export class DadosController {
  constructor(private readonly dadosService: DadosService) {}

  @Post()
  async create(@Body() createDadoDto: CreateDadoDto) {
    return await this.dadosService.create(createDadoDto);
  }

  @Get()
  async findAll() {
    return await this.dadosService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.dadosService.findOne(id);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updateDadoDto: UpdateDadoDto) {
    return await this.dadosService.update(id, updateDadoDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.dadosService.remove(id);
  }

  @Get('plantacao/:id')
  async findAllOfPlantacao(@Param('id') id: string) {
    return await this.dadosService.findAllOfPlantacao(id);
  }
}
