import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Plantacao } from '../../plantacao/entities/plantacao.entity';

@Entity()
export class Dado {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false, type: 'decimal' })
  umidade_solo: number;

  @Column({ nullable: false, type: 'decimal' })
  umidade_ar: number;

  @Column({ nullable: false, type: 'decimal' })
  velocidade_ar: number;

  @Column({ nullable: false, type: 'decimal' })
  temperatura: number;

  @Column({ nullable: false })
  status: boolean;

  @Column({ nullable: false })
  reservatorio: boolean;

  @Column({ nullable: false })
  @Index()
  registro: Date;

  @ManyToOne(() => Plantacao, (plantacao) => plantacao.dados)
  @JoinColumn()
  plantacao: Plantacao;
}
