import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Dado } from './entities/dado.entity';
import { DadosService } from './dados.service';
import { PlantacaoService } from '../plantacao/plantacao.service';

describe('DadosService', () => {
  let service: DadosService;
  let repository: Repository<Dado>;

  const mockCreateDadoDto = {
    id: '1640237604',
    umidade_solo: 50,
    umidade_ar: 25,
    velocidade_ar: 100,
    temperatura: 25,
    status: true,
    reservatorio: true,
    registro: new Date(),
    plantacao: {
      id: '1640237604',
      nome: 'Plantacao 1',
      manual: false,
      status: true,
    },
  };

  const mockReturnDadoDto = {
    ...mockCreateDadoDto,
  };

  const mockDadoEntityList = [{ ...mockReturnDadoDto }];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DadosService,
        {
          provide: getRepositoryToken(Dado),
          useValue: {
            create: jest.fn().mockReturnValue(new Dado()),
            find: jest.fn().mockResolvedValue(mockDadoEntityList),
            findOneBy: jest.fn().mockResolvedValue(mockReturnDadoDto),
            merge: jest.fn().mockReturnValue(mockReturnDadoDto),
            delete: jest.fn(),
            save: jest.fn(),
          },
        },
        {
          provide: PlantacaoService,
          useValue: {
            findOne: jest.fn().mockResolvedValue(mockReturnDadoDto),
          },
        },
      ],
    }).compile();

    service = module.get<DadosService>(DadosService);
    repository = module.get<Repository<Dado>>(getRepositoryToken(Dado));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(repository).toBeDefined();
  });
});
