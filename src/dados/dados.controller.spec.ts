import { Test, TestingModule } from '@nestjs/testing';
import { DadosController } from './dados.controller';
import { DadosService } from './dados.service';
import { CreateDadoDto } from './dto/create-dado.dto';

describe('DadosController', () => {
  let controller: DadosController;

  const mockCreateDadosDto: CreateDadoDto = {
    umidade_solo: 0.5,
    umidade_ar: 0.5,
    velocidade_ar: 0.5,
    temperatura: 0.5,
    status: true,
    reservatorio: true,
    registro: new Date(),
    plantacao_id: '1640237604',
  };

  const mockDadosService = {
    create: jest.fn((dto) => {
      return {
        ...dto,
      };
    }),
    findOne: jest.fn((id) => {
      return {
        ...mockCreateDadosDto,
        id,
      };
    }),
    update: jest.fn((id, dto) => {
      return {
        ...mockCreateDadosDto,
        ...dto,
      };
    }),
    remove: jest.fn(() => {
      return {
        message: 'Dado removido com sucesso',
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DadosController],
      providers: [DadosService],
    })
      .overrideProvider(DadosService)
      .useValue(mockDadosService)
      .compile();

    controller = module.get<DadosController>(DadosController);
  });

  describe('dados', () => {
    it('should be defined', () => {
      expect(controller).toBeDefined();
    });
  });
});
