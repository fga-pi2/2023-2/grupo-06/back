import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateDadoDto } from './dto/create-dado.dto';
import { UpdateDadoDto } from './dto/update-dado.dto';
import { Dado } from './entities/dado.entity';
import { PlantacaoService } from '../plantacao/plantacao.service';

@Injectable()
export class DadosService {
  constructor(
    @InjectRepository(Dado)
    private readonly dadoRepository: Repository<Dado>,
    private readonly plantacaoService: PlantacaoService,
  ) {}

  async create(createDadoDto: CreateDadoDto): Promise<Dado> {
    // console.log(createDadoDto)
    // Search plantacao by plantacao_id using try/catch
    try {
      const plantacao = await this.plantacaoService.findOne(
        createDadoDto.plantacao_id,
      );
      // console.log(plantacao)
      try {
        const dado = this.dadoRepository.create({
          ...createDadoDto,
          plantacao,
        });

        return await this.dadoRepository.save(dado);
      } catch (error) {
        throw new Error('Erro ao criar dado');
      }
    } catch (error) {
      throw new Error('Plantacao não encontrada');
    }
  }

  async findAll(): Promise<Dado[]> {
    return await this.dadoRepository.find();
  }

  async findOne(id: string): Promise<Dado> {
    return await this.dadoRepository.findOneBy({ id: id });
  }

  async update(id: string, updateDadoDto: UpdateDadoDto): Promise<Dado> {
    const dado = await this.dadoRepository.findOneBy({ id: id });
    this.dadoRepository.merge(dado, updateDadoDto);
    return await this.dadoRepository.save(dado);
  }

  async remove(id: string): Promise<void> {
    await this.dadoRepository.delete(id);
  }

  async findAllOfPlantacao(idPlantacao: string): Promise<Dado[]> {
    return await this.dadoRepository.find({
      where: { plantacao: { id: idPlantacao } },
      order: { registro: 'DESC' },
      take: 100,
    });
  }
}
