export class BusinessRuleError extends Error {
  constructor(message) {
    super(message);
  }
}
