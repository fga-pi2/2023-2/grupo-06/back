import { join } from 'path';

interface Config {
  type: 'postgres' | 'sqlite';
  url?: string;
  host?: string;
  port?: number;
  username?: string;
  password?: string;
  database?: string;
  entities: string[];
  synchronize: boolean;
  extra?: {
    ssl: {
      rejectUnauthorized: boolean;
    };
  };
}

const config = (): Config => {
  const isPrd = process.env.ENV === 'production';

  if (isPrd) {
    return {
      type: 'postgres',
      url: process.env.DATABASE_URL,
      entities: [join(__dirname, '..', '**', '*.entity.{js,ts}')],
      synchronize: true,
      extra: {
        ssl: {
          rejectUnauthorized: false,
        },
      },
    };
  }

  return {
    type: 'sqlite',
    database: join(__dirname, '..', '..', '..', 'sqlite', 'database.sqlite'),
    entities: [join(__dirname, '..', '**', '*.entity.{js,ts}')],
    synchronize: true,
  };
};

export default config();
