import { Module } from '@nestjs/common';
import { PlantacaoService } from './plantacao.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Plantacao } from './entities/plantacao.entity';
import { PlantacaoController } from './plantacao.controller';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';

@Module({
  imports: [TypeOrmModule.forFeature([Plantacao, User])],
  controllers: [PlantacaoController],
  providers: [PlantacaoService, UsersService],
})
export class PlantacaoModule {}
