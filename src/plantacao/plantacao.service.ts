import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePlantacaoDto } from './dto/create-plantacao.dto';
import { UpdatePlantacaoDto } from './dto/update-plantacao.dto';
import { Plantacao } from './entities/plantacao.entity';
import { UsersService } from '../users/users.service';

@Injectable()
export class PlantacaoService {
  constructor(
    @InjectRepository(Plantacao)
    private plantacaoRepository: Repository<Plantacao>,
    private userService: UsersService,
  ) {}

  async create(createPlantacaoDto: CreatePlantacaoDto) {
    try {
      const user = await this.userService.getByEmail(
        createPlantacaoDto.user_email,
      );
      try {
        await this.plantacaoExists(createPlantacaoDto.id);
        const plantacao = this.plantacaoRepository.create({
          ...createPlantacaoDto,
          user,
        });
        return this.plantacaoRepository.save(plantacao);
      } catch (error) {
        throw new ConflictException(
          'Erro ao criar a plantacao no banco de dados',
        );
      }
    } catch (error) {
      throw new Error('Não foi possível cadastrar plantação');
    }
  }

  private async plantacaoExists(id: any) {
    const plantacao = await this.findOne(id);
    if (plantacao) {
      throw new ConflictException('Id já está em uso');
    }
  }

  async findAll() {
    return this.plantacaoRepository.find();
  }

  async findOne(id: string) {
    const plantacao = await this.plantacaoRepository.findOneBy({ id: id });
    return plantacao;
  }

  async findAllOfUser(idUser: string) {
    return this.plantacaoRepository.find({
      where: { user: { id: idUser } },
      relations: ['user', 'configuracao'],
    });
  }

  async update(id: string, updatePlantacaoDto: UpdatePlantacaoDto) {
    try {
      const plantacao = await this.plantacaoRepository.findOneBy({ id: id });
      this.plantacaoRepository.merge(plantacao, updatePlantacaoDto);

      return this.plantacaoRepository.save(plantacao);
    } catch (error) {
      throw new Error('Não foi possível atualizar a plantação');
    }
  }

  async remove(id: string) {
    await this.plantacaoRepository.delete(id);
  }
}
