import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  Unique,
} from 'typeorm';
import { Dado } from '../../dados/entities/dado.entity';
import { Configuracao } from '../../configuracao/entities/configuracao.entity';
import { User } from '../../users/user.entity';

@Entity()
@Unique(['id'])
export class Plantacao {
  @PrimaryColumn({ nullable: false })
  id: string;

  @Column({ nullable: false })
  nome: string;

  @Column({ nullable: false })
  manual: boolean;

  @Column({ nullable: false })
  status: boolean;

  @ManyToOne(() => User, (user) => user.plantacao)
  @JoinColumn()
  user: User;

  @OneToMany(() => Dado, (dados) => dados.plantacao)
  dados: Dado;

  @OneToOne(() => Configuracao, (configuracao) => configuracao.plantacao)
  configuracao: Configuracao;
}
