import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Plantacao } from './entities/plantacao.entity';
import { PlantacaoService } from './plantacao.service';
import { CreatePlantacaoDto } from './dto/create-plantacao.dto';
import { UpdatePlantacaoDto } from './dto/update-plantacao.dto';
import { UsersService } from '../users/users.service';

describe('PlantacaoService', () => {
  let service: PlantacaoService;
  let repository: Repository<Plantacao>;

  const mockCreatePlantacaoDto: CreatePlantacaoDto = {
    id: '1640237604',
    nome: 'Plantacao 1',
    user_email: 'teste@email.com',
    manual: false,
    status: true,
  };

  const mockCreateUserDto = {
    id: '1640237604',
    nome: 'User 1',
    email: 'teste@email.com',
    senha: '123456',
  };

  const mockReturnPlantacaoDto = {
    ...mockCreatePlantacaoDto,
  };

  const mockUpdatePlantacaoDto: UpdatePlantacaoDto = {
    ...mockCreatePlantacaoDto,
    manual: true,
  };

  const mockPlantacaoEntityList = [{ ...mockReturnPlantacaoDto }];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PlantacaoService,
        {
          provide: getRepositoryToken(Plantacao),
          useValue: {
            create: jest.fn().mockReturnValue(new Plantacao()),
            find: jest.fn().mockResolvedValue(mockPlantacaoEntityList),
            findOneBy: jest.fn().mockResolvedValue(mockReturnPlantacaoDto),
            merge: jest.fn().mockReturnValue(mockReturnPlantacaoDto),
            delete: jest.fn(),
            save: jest.fn(),
          },
        },
        {
          provide: UsersService,
          useValue: {
            getByEmail: jest.fn().mockResolvedValue(mockCreateUserDto),
          },
        },
      ],
    }).compile();

    service = module.get<PlantacaoService>(PlantacaoService);
    repository = module.get<Repository<Plantacao>>(
      getRepositoryToken(Plantacao),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(repository).toBeDefined();
  });

  // describe('createPlantacao', () => {
  //   const dto = mockCreatePlantacaoDto;
  //   it('should call plantacao repository with correct params', async () => {
  //     await service.create(dto);
  //     expect(repository.create).toHaveBeenCalledWith({
  //       ...dto,
  //       user: mockCreateUserDto,
  //     });
  //     expect(repository.create);
  //   });
  // });

  describe('findAllPlantacao', () => {
    it('should call plantacao repository', async () => {
      await service.findAll();
      expect(repository.find).toHaveBeenCalled();
    });
  });

  describe('findOnePlantacao', () => {
    it('should call plantacao repository with correct params', async () => {
      await service.findOne('1640237604');
      expect(repository.findOneBy).toHaveBeenCalledWith({
        id: '1640237604',
      });
    });
  });

  describe('updatePlantacao', () => {
    const dto = mockUpdatePlantacaoDto;
    it('should call plantacao repository with correct params', async () => {
      await service.update('1640237604', dto);
      expect(repository.findOneBy).toHaveBeenCalledWith({
        id: '1640237604',
      });
      expect(repository.merge).toHaveBeenCalledWith(
        mockReturnPlantacaoDto,
        dto,
      );
      expect(repository.save).toHaveBeenCalledWith(mockReturnPlantacaoDto);
    });
  });

  describe('removePlantacao', () => {
    it('should call plantacao repository with correct params', async () => {
      await service.remove('1640237604');
      expect(repository.delete).toHaveBeenCalledWith('1640237604');
    });
  });
});
