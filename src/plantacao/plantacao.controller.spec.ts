import { Test, TestingModule } from '@nestjs/testing';
import { PlantacaoController } from './plantacao.controller';
import { PlantacaoService } from './plantacao.service';
import { CreatePlantacaoDto } from './dto/create-plantacao.dto';
import { UpdatePlantacaoDto } from './dto/update-plantacao.dto';
import { MqttService } from '../mqtt/mqtt.service';

describe('PlantacaoController', () => {
  let controller: PlantacaoController;

  const mockCreatePlantacaoDto: CreatePlantacaoDto = {
    id: '1640237604',
    nome: 'Plantacao 1',
    user_email: 'teste@email.com',
    manual: false,
    status: false,
  };

  const mockUpdatePlantacaoDto: UpdatePlantacaoDto = {
    nome: 'Plantacao 2_mock_teste',
    user_email: 'teste1@email.com',
    manual: true,
    status: true,
  };

  const mockPlantacaoService = {
    create: jest.fn((dto) => {
      return {
        ...dto,
      };
    }),
    findOne: jest.fn((id) => {
      return {
        ...mockCreatePlantacaoDto,
        id,
      };
    }),
    update: jest.fn((id, dto) => {
      return {
        ...mockCreatePlantacaoDto,
        ...dto,
      };
    }),
    remove: jest.fn(() => {
      return {
        message: 'Plantacao removida com sucesso',
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlantacaoController],
      providers: [PlantacaoService, { provide: MqttService, useValue: {} }],
    })
      .overrideProvider(PlantacaoService)
      .useValue(mockPlantacaoService)
      .compile();

    controller = module.get<PlantacaoController>(PlantacaoController);
  });

  describe('plantacao', () => {
    it('should be defined"', () => {
      expect(controller).toBeDefined();
    });

    it('should create an plantacao with success', async () => {
      const dto = mockCreatePlantacaoDto;
      const response = await controller.create(dto);

      expect(response).toMatchObject({ ...dto });
    });

    it('should return an plantacao with success', async () => {
      const dto = mockCreatePlantacaoDto;
      const response = await controller.findOne(dto.id);

      expect(response).toMatchObject({ ...dto });
    });

    it('should update an plantacao with success', async () => {
      const dto = mockUpdatePlantacaoDto;
      const response = await controller.update(mockCreatePlantacaoDto.id, dto);

      expect(response).toMatchObject({ ...mockCreatePlantacaoDto, ...dto });
    });

    it('should delete an plantacao with success', async () => {
      const dto = mockCreatePlantacaoDto;
      const response = await controller.remove(dto.id);
      const successMessage = 'Plantacao removida com sucesso';

      expect(response).toMatchObject({ message: successMessage });
    });
  });
});
