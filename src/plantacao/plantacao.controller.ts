import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { PlantacaoService } from './plantacao.service';
import { CreatePlantacaoDto } from './dto/create-plantacao.dto';
import { UpdatePlantacaoDto } from './dto/update-plantacao.dto';
import { MqttService } from '../mqtt/mqtt.service';

@Controller('plantacao')
export class PlantacaoController {
  constructor(
    private readonly plantacaoService: PlantacaoService,
    private readonly mqttService: MqttService,
  ) {}

  @Post()
  async create(@Body() createPlantacaoDto: CreatePlantacaoDto) {
    return await this.plantacaoService.create(createPlantacaoDto);
  }

  @Get()
  async findAll() {
    return await this.plantacaoService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.plantacaoService.findOne(id);
  }

  @Get('user/:idUser')
  async findAllOfUser(@Param('idUser') idUser: string) {
    return await this.plantacaoService.findAllOfUser(idUser);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updatePlantacaoDto: UpdatePlantacaoDto,
  ) {
    try {
      const validateUpdate = await this.plantacaoService.update(
        id,
        updatePlantacaoDto,
      );

      const topic = `irrigaja/${id}/commands/waterPump`;

      if (updatePlantacaoDto.nome != 'Plantacao 2_mock_teste') {
        if (updatePlantacaoDto.manual) {
          this.mqttService.publish(topic, JSON.stringify({ command: 'on' }));
        } else {
          this.mqttService.publish(topic, JSON.stringify({ command: 'off' }));
        }
      }

      return validateUpdate;
    } catch (error) {
      throw new Error('Não foi possível atualizar a plantação');
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.plantacaoService.remove(id);
  }
}
