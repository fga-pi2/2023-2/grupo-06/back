import { IsBoolean, IsNotEmpty, MaxLength } from 'class-validator';

export class CreatePlantacaoDto {
  @IsNotEmpty({ message: 'Insira o id da ESP conectada a plantacao' })
  id: string;

  @IsNotEmpty({ message: 'Insira um nome para a plantacao' })
  @MaxLength(200, { message: 'O nome deve ter no máximo 200 caracteres' })
  nome: string;

  @IsNotEmpty({ message: 'Insira um valor para o manual' })
  @IsBoolean({ message: 'O campo manual deve ser booleano' })
  manual: boolean;

  @IsNotEmpty({ message: 'Insira um valor para o status' })
  @IsBoolean({ message: 'O campo status deve ser booleano' })
  status: boolean;

  @IsNotEmpty({ message: 'Insira o e-mail do usuario' })
  user_email: string;
}
