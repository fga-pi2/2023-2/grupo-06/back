import { PartialType } from '@nestjs/mapped-types';
import { CreatePlantacaoDto } from './create-plantacao.dto';
import { IsBoolean } from 'class-validator';

export class UpdatePlantacaoDto extends PartialType(CreatePlantacaoDto) {
  @IsBoolean({ message: 'O campo manual deve ser booleano' })
  manual: boolean;

  @IsBoolean({ message: 'O campo status deve ser booleano' })
  status: boolean;
}
