import { Injectable } from '@nestjs/common';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BusinessRuleError } from '../errors/BusinessRuleError';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async createUser(payload: { nome: string; email: string; senha: string }) {
    if (!this.isEmailValid(payload.email))
      throw new BusinessRuleError('Email inválido.');
    this.validatePassword(payload.senha);
    await this.userEmailExists(payload.email);
    payload.senha = await this.hashPassword(payload.senha);
    await this.userRepository.insert(payload);
  }

  async signIn(email: string, password: string) {
    const user = await this.getByEmail(email);
    console.log(user);
    if (!user) throw new BusinessRuleError('Usuário não encontrado.');
    const passwordIsValid = await bcrypt.compare(password, user.senha);
    if (!passwordIsValid) throw new BusinessRuleError('Credenciais inválidas.');
    delete user.senha;
    const token = jwt.sign(JSON.stringify(user), process.env.JWT_SECRET, {});
    return {
      token,
      user,
    };
  }

  async getByEmail(email: string) {
    if (!this.isEmailValid(email))
      throw new BusinessRuleError('Email inválido.');
    const user = await this.userRepository.findOne({ where: { email } });

    return user;
  }

  async findOne(id: string) {
    return this.userRepository.findOneBy({ id: id });
  }

  async update(email: string, updateUserDto: { nome: string; senha: string }) {
    const user = await this.getByEmail(email);
    if (!user) throw new BusinessRuleError('Usuário não encontrado.');
    if (updateUserDto.nome) user.nome = updateUserDto.nome;
    if (updateUserDto.senha)
      user.senha = await this.hashPassword(updateUserDto.senha);
    return this.userRepository.save(user);
  }

  private isEmailValid(email: string) {
    // Define uma expressão regular para verificar o formato do email
    const emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;

    // Use o método test() da regex para verificar se a string corresponde ao padrão de email
    return emailRegex.test(email);
  }

  private validatePassword(password: string) {
    if (typeof password !== 'string') {
      throw new BusinessRuleError('A senha deve ser uma string');
    }

    if (password.length < 8) {
      throw new BusinessRuleError('A senha deve ter 8 ou mais caracteres');
    }

    if (!/[A-Z]/.test(password)) {
      throw new BusinessRuleError(
        'A senha deve conter pelo menos uma letra maiúscula',
      );
    }

    if (!/\d/.test(password)) {
      throw new BusinessRuleError('A senha deve conter pelo menos um número');
    }

    if (!/^[A-Za-z0-9]*$/.test(password)) {
      throw new BusinessRuleError(
        'A senha deve conter apenas caracteres alfanuméricos',
      );
    }
    return true;
  }

  private async userEmailExists(email: any) {
    const user = await this.getByEmail(email);
    if (user)
      throw new BusinessRuleError(
        'Já existe um usuário cadastrado com esse e-mail.',
      );
  }

  private async hashPassword(password: string) {
    const saltRounds = 12; // Número de "rounds" de salt (mais rounds, mais seguro, mas mais lento)
    try {
      const hashedPassword = await bcrypt.hash(password, saltRounds);
      return hashedPassword;
    } catch (error) {
      throw new Error('Erro ao criar hash da senha: ' + error.message);
    }
  }
}
