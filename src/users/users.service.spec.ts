import { Test } from '@nestjs/testing';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('UsersService', () => {
  let service: UsersService;
  let repository: Repository<User>;

  const mockCreateUserDto = {
    id: '1640237604',
    nome: 'User 1',
    email: 'teste@email.com',
    senha: '123456',
  };

  const mockReturnUserDto = {
    ...mockCreateUserDto,
  };

  const mockUserEntityList = [{ ...mockReturnUserDto }];

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            create: jest.fn().mockReturnValue(new User()),
            find: jest.fn().mockResolvedValue(mockUserEntityList),
            findOneBy: jest.fn().mockResolvedValue(mockReturnUserDto),
            merge: jest.fn().mockReturnValue(mockReturnUserDto),
            delete: jest.fn(),
            save: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get(UsersService);
    repository = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(repository).toBeDefined();
  });
});
