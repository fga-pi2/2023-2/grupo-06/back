import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Plantacao } from '../plantacao/entities/plantacao.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false, type: 'varchar', length: '100' })
  nome: string;

  @Column({ nullable: false, type: 'varchar', length: '100' })
  email: string;

  @Column({ nullable: false, type: 'varchar' })
  senha: string;

  @OneToMany(() => Plantacao, (plantacao) => plantacao.user)
  plantacao: Plantacao;
}
