import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('Users Controller', () => {
  let controller: UsersController;

  const mockCreateUserDto = {
    id: '1640237604',
    nome: 'User 1',
    email: 'teste@email.com',
    senha: '123456',
  };

  const mockUsersService = {
    create: jest.fn((dto) => {
      return {
        ...dto,
      };
    }),
    findOne: jest.fn((id) => {
      return {
        ...mockCreateUserDto,
        id,
      };
    }),
    update: jest.fn((id, dto) => {
      return {
        ...mockCreateUserDto,
        ...dto,
      };
    }),
    remove: jest.fn(() => {
      return {
        message: 'Usuário removido com sucesso',
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
    })
      .overrideProvider(UsersService)
      .useValue(mockUsersService)
      .compile();

    controller = module.get<UsersController>(UsersController);
  });

  describe('users', () => {
    it('should be defined', () => {
      expect(controller).toBeDefined();
    });
  });
});
