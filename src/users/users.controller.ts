import {
  Controller,
  Post,
  Get,
  Body,
  Bind,
  Dependencies,
  HttpException,
  HttpStatus,
  Logger,
  Param,
  Put,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { BusinessRuleError } from '../errors/BusinessRuleError';

@Controller('users')
@Dependencies(UsersService)
export class UsersController {
  private logger = new Logger();
  constructor(private usersService: UsersService) {}

  /**
   *
   * @param {{nome: string, email: string, senha: string}} createUserPayload
   * @returns
   */
  @Post('sign-up')
  @Bind(Body())
  async signUp(createUserPayload: {
    nome: string;
    email: string;
    senha: string;
  }) {
    try {
      await this.usersService.createUser(createUserPayload);

      return 'Usuário criado';
    } catch (error) {
      this.logger.error(error);
      if (error instanceof BusinessRuleError) {
        throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
      }

      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  @Post('sign-in')
  @Bind(Body())
  async signIn(credentials: { email: string; senha: string }) {
    try {
      console.log(credentials);

      return await this.usersService.signIn(
        credentials.email,
        credentials.senha,
      );
    } catch (error) {
      if (error instanceof BusinessRuleError) {
        this.logger.error(error);
        throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
      }

      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  @Get()
  @Bind(Body())
  async getByEmail(payload: { email: string }) {
    try {
      return await this.usersService.getByEmail(payload.email);
    } catch (error) {
      if (error instanceof BusinessRuleError) {
        this.logger.error(error);
        throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
      }

      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  @Put('sign-in/:email')
  async update(
    @Param('email') email: string,
    @Body() payload: { nome: string; senha: string },
  ) {
    return this.usersService.update(email, payload);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(id);
  }
}
